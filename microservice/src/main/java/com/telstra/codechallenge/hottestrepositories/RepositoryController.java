package com.telstra.codechallenge.hottestrepositories;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author nagendra.kola
 * 
 *         This class is acts as controller and used to call service.
 *
 */
@RestController
@Slf4j
public class RepositoryController {

	@Autowired
	RepositoryService repositoryService;

	@GetMapping(value = "search", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ItemDTO>> getRepositories(@RequestParam("size") Integer size) {
		log.info("Searching repositories details");
		return repositoryService.getAllRepositories(size);
	}

}
