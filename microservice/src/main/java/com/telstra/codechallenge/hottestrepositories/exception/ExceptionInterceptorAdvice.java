package com.telstra.codechallenge.hottestrepositories.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * 
 * @author nagendra.kola
 *
 */
@ControllerAdvice
public class ExceptionInterceptorAdvice extends ResponseEntityExceptionHandler {

	@ExceptionHandler(GitHubRepositoryException.class)
	public final ResponseEntity<Object> handleRepositoryExceptions(GitHubRepositoryException ex) {
		CustomExceptionFormatter exceptionResponse = new CustomExceptionFormatter(ex.getMessage(), ex.getDetails());
		return new ResponseEntity<Object>(exceptionResponse, HttpStatus.BAD_REQUEST);
	}

}
