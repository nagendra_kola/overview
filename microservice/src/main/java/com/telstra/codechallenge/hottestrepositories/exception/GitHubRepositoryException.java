package com.telstra.codechallenge.hottestrepositories.exception;

import lombok.EqualsAndHashCode;
import lombok.Value;

/**
 * 
 * @author nagendra.kola
 *
 */

@Value
@EqualsAndHashCode(callSuper = false)
public class GitHubRepositoryException extends RuntimeException {
	private static final long serialVersionUID = 7718828512143293558L;
	String message;
	String details;

	public GitHubRepositoryException(String message, String details) {
		this.message = message;
		this.details = details;
	}
}
