package com.telstra.codechallenge.hottestrepositories.util;

public class PathVariablesConstants {

	public static final String PATH_START = "q";
	public static final String SORT = "sort";
	public static final String ORDER = "order";
	public static final String PAGE = "page";
	public static final String PER_PAGE = "per_page";
	public static final String WEEK_FIRST_MORE = "created:>=";
	public static final String WEEK_LAST_LESS = "created:<";
	public static final String CUSTOM_EXCEPTION = "Something went wrong";

}
