package com.telstra.codechallenge;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import com.telstra.codechallenge.hottestrepositories.ItemDTO;
import com.telstra.codechallenge.hottestrepositories.RepositoryController;
import com.telstra.codechallenge.hottestrepositories.RepositoryService;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class RepositoryControllerTest {

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;
	@InjectMocks
	private RepositoryController repositoryController;
	@MockBean
	private RepositoryService repositoryService;

	List<ItemDTO> mockItemDTO = Arrays.asList(
			new ItemDTO("https://github.com/karpathy/minGPT", 2533L, "Jupyter Notebook", "A minimal PyTorch training",
					"minGPT"),
			new ItemDTO("https://github.com/BeichenDream/Godzilla", 287L, "java", "A  training", "Godzilla"),
			new ItemDTO("https://github.com/xyzzy/qrpicture", 281L, "C++",
					"Picture to QR code converter hosted on www.QRpicture.com", "qrpicture"));

	@Test
	public void testHealth() throws RestClientException, MalformedURLException {
		ResponseEntity<String> response = restTemplate
				.getForEntity(new URL("http://localhost:" + port + "/actuator/health").toString(), String.class);
		assertEquals("{\"status\":\"UP\"}", response.getBody());
	}

	@Test
	void testController() {
		Mockito.when(repositoryService.getAllRepositories(3))
				.thenReturn(new ResponseEntity<List<ItemDTO>>(mockItemDTO, HttpStatus.OK));
		ItemDTO[] response = this.restTemplate.getForObject("/search?size=3", ItemDTO[].class);
		assertNotNull(response);
		assertNotEquals(5, response.length);
		assertEquals(3, response.length);
	}

}
