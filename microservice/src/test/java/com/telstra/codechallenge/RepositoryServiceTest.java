package com.telstra.codechallenge;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;
import com.telstra.codechallenge.hottestrepositories.ItemDTO;
import com.telstra.codechallenge.hottestrepositories.RepositoryDTO;
import com.telstra.codechallenge.hottestrepositories.RepositoryService;
import com.telstra.codechallenge.hottestrepositories.exception.ExceptionInterceptorAdvice;
import com.telstra.codechallenge.hottestrepositories.exception.GitHubRepositoryException;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class RepositoryServiceTest {

	@LocalServerPort
	private int port;

	@Value("${github.reposioty.base.url}")
	private String rootURI;

	@Value("${github.repository.sort.value}")
	private String sortValue;

	@Value("${github.repository.order.value}")
	private String orderValue;

	@Value("${github.repository.default.pageSize}")
	private int defaultValue;

	@Mock
	private RestTemplate restTemplate;

	@MockBean
	private RepositoryDTO repositoryDTO;

	@MockBean
	private ItemDTO itemDTO;

	@InjectMocks
	private RepositoryService repositoryService;

	@MockBean
	ExceptionInterceptorAdvice exceptionInterceptorAdvice;

	@BeforeEach
	public void setUp() throws Exception {

		ReflectionTestUtils.setField(repositoryService, "defaultValue", defaultValue);
		ReflectionTestUtils.setField(repositoryService, "rootURI", rootURI);
		ReflectionTestUtils.setField(repositoryService, "sortValue", sortValue);
		ReflectionTestUtils.setField(repositoryService, "orderValue", orderValue);
	}

	@Test
	public void testService() {
		List<ItemDTO> mockItemDTO = Arrays.asList(
				new ItemDTO("https://github.com/karpathy/minGPT", 2533L, "Jupyter Notebook",
						"A minimal PyTorch training", "minGPT"),
				new ItemDTO("https://github.com/BeichenDream/Godzilla", 287L, "java", "A  training", "Godzilla"),
				new ItemDTO("https://github.com/xyzzy/qrpicture", 281L, "C++",
						"Picture to QR code converter hosted on www.QRpicture.com", "qrpicture"));

		String url = "https://api.github.com/search/repositories?q=created:>=2020-08-10&created:<=2020-08-16&sort=stars&order=desc&page=1&per_page=3";
		RepositoryDTO mockRepositoryDTO = new RepositoryDTO(mockItemDTO);
		Mockito.when(restTemplate.getForEntity(url, RepositoryDTO.class))
				.thenReturn(new ResponseEntity<RepositoryDTO>(mockRepositoryDTO, HttpStatus.OK));
		ResponseEntity<List<ItemDTO>> response = repositoryService.getAllRepositories(3);
		assertEquals(200, response.getStatusCodeValue());
		assertNotNull(response.getBody());
		assertEquals(3, response.getBody().size());
	}

	@Test
	public void testServicewithException() throws GitHubRepositoryException {
		GitHubRepositoryException userException = new GitHubRepositoryException("Something went Wrong",
				"Something went wrong");
		ResponseEntity<Object> response = exceptionInterceptorAdvice.handleRepositoryExceptions(userException);
		assertEquals(400, response.getStatusCodeValue());
	}
}
